package com.example.k8testproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K8TestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(K8TestProjectApplication.class, args);
	}

}
